<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Revista extends Model
{
    protected $fillable = [
        'idrevista','idcategoria','nombre','descripcion','precio','video1','video2','imagen1','imagen2','condicion'
    ];
    public function categoria(){
        return $this->hasMany('App\Revista');
    }
}
