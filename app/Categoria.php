<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $fillable = ['categoria','sub_categoria'];
    public function revistas(){
        return $this->belongsTo('App\Categoria');
    }
}
