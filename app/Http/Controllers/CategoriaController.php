<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use PhpParser\Node\Expr\New_;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = Categoria::all();
        return $categorias;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $categoria= new Categoria();
        $categoria->categoria=$request->categoria;
        $categoria->sub_categoria=$request->sub_categoria;
        $categoria->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $categoria=  Categoria::findOrFail($request->id);
        $categoria->categoria=$request->categoria;
        $categoria->sub_categoria=$request->sub_categoria;
        $categoria->save();
    }

    public function desactivar(Request $request)
    {
        //
    }

    public function activar(Request $request)
    {
        //
    }


}
