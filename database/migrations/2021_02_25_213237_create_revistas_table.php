<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRevistasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revistas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('idrevista',100);
            $table->integer('idcategoria')->unsigned();
            $table->string('nombre',100);
            $table->longText('descripcion');
            $table->string('precio',50);
            $table->string('video1',250)->nullable();
            $table->string('video2',250)->nullable();
            $table->string('imagen1',250)->nullable();
            $table->string('imagen2',250)->nullable();
            $table->boolean('condicion')->default(1);
            $table->timestamps();

            $table->foreign('idcategoria')->references('id')->on('categorias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('revistas');
    }
}
