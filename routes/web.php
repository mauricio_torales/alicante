<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('web.welcome');
});



//admin
Route::get('/admin', function () {
    return view('admin.contenido.contenido');
});

Route::get('/admin/categorias','CategoriaController@index');
Route::post('/admin/categorias/add','CategoriaController@store');
Route::put('/admin/categorias/update','CategoriaController@update');


Route::get('/admin/revistas','RevistaController@index');
Route::get('/admin/revistas/add','RevistaController@store');
