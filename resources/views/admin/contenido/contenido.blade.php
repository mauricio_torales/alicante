@extends('admin.principal')
@section('contenido')
    <template v-if="menu==0" >
        <inicio></inicio>
    </template>
    <template v-if="menu==1" >
        <categoria></categoria>
    </template>
    <template v-if="menu==2" >
        <revistas></revistas>
    </template>
@endsection
