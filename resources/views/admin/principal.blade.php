<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Sistema Ventas Laravel Vue Js- IncanatoIT">
    <meta name="author" content="Incanatoit.com">
    <meta name="keyword" content="Sistema ventas Laravel Vue Js, Sistema compras Laravel Vue Js">

    <title>Administrador Alicante</title>
    <!-- Icons -->
    <link href="css/admin.css" rel="stylesheet">

</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
    <div id="app">


        <header class="app-header navbar">
            <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button">
            <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="#"></a>
            <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button">
            <span class="navbar-toggler-icon"></span>
            </button>
            <ul class="nav navbar-nav d-md-down-none">
                <li class="nav-item px-3">
                    <a class="nav-link" href="#">Ir a la tienda</a>
                </li>

            </ul>
            <ul class="nav navbar-nav ml-auto" style="margin-right: 30px">
                <li class="nav-item ">
                    <a class="nav-link " >
                        <img src="https://cdn4.iconfinder.com/data/icons/avatars-xmas-giveaway/128/batman_hero_avatar_comics-512.png" class="img-avatar" alt="admin@bootstrapmaster.com">
                        <span class="d-md-down-none">admin </span>
                    </a>
                </li>
            </ul>
        </header>

        <div class="app-body">

                @include('admin.componentes.sidebar')

            <!-- Contenido Principal -->
                @yield('contenido')
            <!-- /Fin del contenido principal -->
        </div>

    </div>


    <script src="js/app.js"></script>
    <!-- Bootstrap and necessary plugins -->
    <script src="js/admin.js"></script>


</body>

</html>
